﻿using System;
using System.IO;
using System.Linq;
using System.Windows.Forms;

namespace split_tool
{
    class Program
    {
        static void Main(string[] args)
        {
            if ((args.Length != 1) && (args.Length != 4))
            {
                error();
            }

            if (args.Length == 4)
            {
                ulong startOffset = 0, endOffset = 0;

                try
                {
                    if (args[0].ToLower().Contains("x"))
                        startOffset = Convert.ToUInt64(args[0], 16);
                    else if (args[0].ToLower().Contains("start"))
                        startOffset = 0;
                    else if (args[0].ToLower().Contains("end"))
                        startOffset = (ulong)(new FileInfo(args[2]).Length);
                    else
                        startOffset = UInt64.Parse(args[0]);

                    if (args[1].ToLower().Contains("x"))
                        endOffset = Convert.ToUInt64(args[1], 16);
                    else if (args[1].ToLower().Contains("start"))
                        endOffset = 0;
                    else if (args[1].ToLower().Contains("end"))
                        endOffset = (ulong)(new FileInfo(args[2]).Length);
                    else
                        endOffset = UInt64.Parse(args[1]);
                }
                catch (ArgumentException) { error(); }

                if (startOffset < 0 || endOffset < 0)
                    error();

                if (endOffset >= startOffset)
                    ForwardReading(args, startOffset, endOffset);
                else
                    ReverseReading(args, startOffset, endOffset);
            }

            else
                error();
        }

        private static void ReverseReading(string[] args, ulong startOffset, ulong endOffset)
        {
            using (FileStream streamR = new FileStream(args[2], FileMode.Open, FileAccess.Read))
            using (FileStream streamW = new FileStream(args[3], FileMode.Create, FileAccess.Write))
            using (BinaryReader reader = new BinaryReader(streamR))
            using (BinaryWriter writer = new BinaryWriter(streamW))
            {

                if (endOffset > int.MaxValue)
                {
                    // Slow but reliable
                    streamR.Seek((long)startOffset, SeekOrigin.Begin);

                    while (streamR.Position >= (long)endOffset)
                    {
                        byte @byte = reader.ReadByte();

                        writer.Write(@byte);

                        streamR.Seek(-2, SeekOrigin.Current);
                    }
                }
                else
                {
                    // Faster. Less reliable - breaks with very large files
                    byte[] buffer = new byte[4096];
                    long position = (long)startOffset + 1;
                    int length = buffer.Length;

                    long toRead = (long)(startOffset - endOffset);
                    long totalRead = 0;

                    while (position > (long)endOffset)
                    {
                        if (position < buffer.Length)
                            length = (int)position;
                        else
                            length = buffer.Length;

                        position -= length;
                        streamR.Seek(position, SeekOrigin.Begin);

                        int bytesRead = reader.Read(buffer, 0, length);
                        totalRead += bytesRead;

                        byte[] reversed;
                        if (totalRead <= toRead)
                            reversed = buffer.Take(bytesRead).Reverse().ToArray();
                        else
                        {
                            bytesRead -= (int)(totalRead - toRead) - 1;
                            reversed = buffer.Take(bytesRead + (int)endOffset).Reverse().ToArray();
                        }

                        writer.Write(reversed, 0, bytesRead);
                    }
                }
            }
        }

        private static void ForwardReading(string[] args, ulong startOffset, ulong endOffset)
        {
            using (FileStream streamR = new FileStream(args[2], FileMode.Open, FileAccess.Read))
            using (FileStream streamW = new FileStream(args[3], FileMode.Create, FileAccess.Write))
            using (BinaryReader reader = new BinaryReader(streamR))
            using (BinaryWriter writer = new BinaryWriter(streamW))
            {
                streamR.Seek((long)startOffset, SeekOrigin.Begin);
                if (streamR.Position == streamR.Length)
                    streamR.Position--;

                ulong toRead = (endOffset - startOffset) + 1;

                byte[] buffer = new byte[4096];

                int readBytes = 0;
                ulong totalRead = 0;
                while ((readBytes = reader.Read(buffer, 0, buffer.Length)) > 0)
                {
                    totalRead += (ulong)readBytes;

                    if (totalRead < toRead)
                        writer.Write(buffer, 0, readBytes);
                    else if (toRead > (ulong)buffer.Length)
                    {
                        writer.Write(buffer, 0, (int)((ulong)readBytes - (totalRead - toRead)));
                        break;
                    }
                    else
                    {
                        writer.Write(buffer, 0, (int)toRead);
                        break;
                    }
                }
            }
        }

        private static void error()
        {
            Console.WriteLine(String.Format("{0} {1}, written by {2} for catalinnc" + Environment.NewLine, Application.ProductName, Application.ProductVersion, Application.CompanyName));

            Console.WriteLine("Usage: {0} starting_offset ending_offset source.bin destination.bin" + Environment.NewLine, Application.ProductName);
            Console.WriteLine("Example: {0} 0xD 0x14 source.bin destination.bin", Application.ProductName);
            Console.WriteLine("         {0} 8 300 source.bin destination.bin", Application.ProductName);

            Environment.Exit(0);
        }
    }
}
